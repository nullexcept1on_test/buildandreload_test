# Build and reload
[![forthebadge made-with-python](http://ForTheBadge.com/images/badges/made-with-python.svg)](https://www.python.org/)

![](https://img.shields.io/badge/version-0.1a-red)

Python dependencies:
[mcrcon](https://pypi.org/project/mcrcon/)

Использование без установки дополнительных модулей из pip:
```Shell
# войти
source dev/bin/activate
# выйти
deactivate
```

------------------
Icon made by [Eucalyp](https://www.flaticon.com/authors/eucalyp) from [www.flaticon.com](https://www.flaticon.com/)